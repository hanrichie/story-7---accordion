function changeTheme() {
    if(document.body.style.backgroundColor == "lightyellow") {
        document.body.style.backgroundColor = "dimgray";
        document.getElementById("btnChange").classList.replace('btn-warning', 'btn-dark');
    } else {
        document.body.style.backgroundColor = "lightyellow";
        document.getElementById("btnChange").classList.replace('btn-dark', 'btn-warning');
    }
}
