from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .views import home
from .apps import HomepageConfig

import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_form_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_app_name(self):
        self.assertEqual(HomepageConfig.name, 'HomePage')
        self.assertEqual(apps.get_app_config('HomePage').name, 'HomePage')